const express = require('express');
const path = require('path');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const routerSuggestion = require('express').Router();
const model = require("../model/datahandler");

routerSuggestion.get('/generate_csv/suggestions.csv', function(req, res) {
	res.sendFile(path.join(__dirname, '../suggestions.csv'));
});

routerSuggestion.post('/save_suggestion', function(req, res) {
    //var myobj = { messenger_id: (Math.floor(Math.random() * 9000000000) + 1000000000).toString(), suggestion: "duiasgiuabigbai cuasgiucgaifg ciuasguicaiugai iuaguif" };
    var myobj = req.body;
    model.save_data("suggestions", myobj);
    const csvWriter = createCsvWriter({
        header: [
        	{id: 'messenger_id', title: 'messenger_id'},
        	{id: 'suggestion', title: 'suggestion'}
        ],
        path: path.join(__dirname, '../suggestions.csv'),
        append: true
    });
const records = [myobj];
 
csvWriter.writeRecords(records)       // returns a promise
    .then(() => {
        console.log('...Done');
    });
res.send("OK");
});

routerSuggestion.get('/suggestions', function(req, res) {
    var page = req.query.page === 'undefined'? 0 : parseInt(req.query.page);
    model.fetch_suggestions(
        function (result, total) {
           res.render('../webview/suggest_list.html',{result:JSON.stringify(result), page:page, pageTot:total});
    	}, page);

});


module.exports = routerSuggestion;